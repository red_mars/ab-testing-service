const optimizely = require('@optimizely/optimizely-sdk');
const Router = require('./router');

const fallbackDatafile = {};

addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request));
});

// returns object where each key is the name of an experiment and each value is the assigned bucket
const getVariationsObject = (instance, { userAttributes, experimentConstants }) => {
  const experiments =
    Object.keys(experimentConstants)
      .map(key => {
        const { feature, name } = experimentConstants[key];
        // the object will either be an a/b experiment, or a feature
        if (feature) {
          // if its a feature, see if its enabled AND return all of its variables
          // TODO: add feature support to this POC once A/B is stable
          return null;
        } else if (name) {
          // if its an ab, get the assignment and return
          return [
            name,
            instance.getVariation(experimentConstants[key].name, userAttributes.userId, userAttributes)
          ];
        } else {
          return null;
        }
      })
      .filter(item => item !== null);
  return Object.fromEntries(experiments);
};

const getInstance = async () => {
  // get the datafile
  // TODO: add fallback datafile
  let datafile = fallbackDatafile;
  try {
    const datafileResponse = await fetch('https://cdn.optimizely.com/datafiles/LBP4Tr1buQJuQ83kCP7YV3.json');
    datafile = await datafileResponse.json();
  } catch (err) {
    console.error('something went wrong retrieving the datafile from optimizely ', err);
  }

  // TODO: cache the datafile? how do we do that? in cloudflare?
  // Force Cloudflare to cache an asset?
  // fetch(event.request, { cf: { cacheEverything: true } });

  // create optimizely instance
  // TODO: how to handle multiple environments? will one instance have to be made for each environment, since the datafiles differ?
  const instance = optimizely.createInstance({
    datafile: datafile,
    eventBatchSize: 1, // don't batch, helps immediately send events
    eventFlushInterval: 0, // immediately send events
    eventDispatcher: {
      dispatchEvent: (eventObj, callback) => {
        const { url, params } = eventObj;
        // using fetch, because the optimizely sdk tries to use XMLHttpRequest which doesn't exist in cloudflare workers
        console.log('eventDispatcher was used');
        fetch(url, {
          headers: { 'content-type': 'application/json' },
          method: 'POST',
          body: JSON.stringify(params),
        });
      }
    },
  });
  return instance;
  // TODO: error handling. need to presumably send sentry errors when things go wrong.
};

/**
 * Provides list of all a user's variations
 * @param   {Request} request
 *
 * @returns {Response} json response
 */
const getVariations = async (request) => {
  const optimizely = await getInstance();
  const postBody = await request.json();
  const variations = getVariationsObject(optimizely, postBody);
  console.log(variations);
  response = new Response(JSON.stringify(variations), { headers: { 'content-type': 'application/json' } });
  return response;
}

const sleep = (ms) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, ms);
  });
}

const sendImpression = async (request) => {
  const optimizely = await getInstance();
  const postBody = await request.json();
  const { expName, userAttributes } = postBody;
  const activationResult = optimizely.activate(expName, userAttributes.userId, userAttributes);
  // stop gap until we can wait until the activate method has been sent before proceeding
  // currently, if we proceed too quick, the response is sent, but the api call to optimizely
  // called from optimizely.activate, is never sent
  await sleep(500);
  console.log('activationResult ', activationResult);
  // TODO: let client know if it was successful or not
  return new Response('trying to log an impression');
};

const trackEvent = async (request) => {
  const optimizely = await getInstance();
  const postBody = await request.json();
  const { eventName, userAttributes } = postBody;
  optimizely.track(eventName, userAttributes.userId, userAttributes);
  await sleep(500);
  // TODO: let client know if it was successful or not
  return new Response('trying to send an event');
};

/**
 * @param {Request} request
 */
async function handleRequest(request) {
  const r = new Router();
  r.post('.*/variations', (req) => getVariations(req));
  r.post('.*/impression', (req) => sendImpression(req));
  r.post('.*/track', (req) => trackEvent(req));
  r.get('/', () => new Response('Hello worker! This is the base path')) // return a default message for the root route
  const response = await r.route(request);
  return response;
}
