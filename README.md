# 👷 `worker-template` Hello World

A template for kick starting a Cloudflare worker project.

[`index.js`](https://github.com/cloudflare/worker-template/blob/master/index.js) is the content of the Workers script.

#### Wrangler

To generate using [wrangler](https://github.com/cloudflare/wrangler)

```
wrangler generate projectname https://github.com/cloudflare/worker-template
```

#### Serverless

To deploy using serverless add a [`serverless.yml`](https://serverless.com/framework/docs/providers/cloudflare/) file.


Learnings/Notes

- installed wrangler so that I could easily create projects locally based off of templates.
- i chose a basic js template, however I ran into an issue. when trying to publish the changes to my worker on cloudflare, it would not allow modules to be imported. I still don't know why this is, because the examples in their own documentation do this.
- the workaround for this so far, has been to change the 'type' in the wrangler.toml to webpack.
- doing that is all thats needed to trigger a webpack build when publishing through wrangler. wrangler is set up to create a 'webworker' and look at the index.js file to compile
- after doing that, the code seems to be able to run, however the down side is that now I can't read it when on the cloudflare dashboard, because its compiled


- what were the results of the impression fetch/xmlhttprequest/http testing? why werent the requests being made?
- there were 2 issues
- first, because the impression event fires asynchronously, it seems the worker finishes its work and stops working before the event has time to fire. WAIT: THIS IS NOT CONFIRMED YET. NEED TO TEST WITH CUSTOM DISPATCHER FIRST AND MAKE SURE THE EVENT STILL ISN'T FIRED
- second, the optimizely sdk is trying to use the browser implementation of the eventDispatcher inside the cloudflare worker. this default eventDispatcher tries to use XMLHttpRequest, which is not accessible from our worker. instead, we need to implement our own custom eventDispatcher using fetch, which IS available. after doing that, I can now see the event being sent, and the impression count in optimizely goes up